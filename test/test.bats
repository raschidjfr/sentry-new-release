#!/usr/bin/env bats

# NOTE: SENTRY_AUTH_TOKEN has the `project:releases` scope.

FIXTURE_DIR="$(pwd)"
ORGANIZATION="some-org"
PROJECT="project"

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/sentry-releases-pipe"}

  echo "Building image $DOCKER_IMAGE..."
  docker build -t ${DOCKER_IMAGE} .
}

teardown() {
  echo "Teardown after each test"
}


@test "Test fails with no token" {
    run docker run \
        -e SENTRY_ORG="${ORGANIZATION}" \
        -e SENTRY_PROJECT="${PROJECT}" \
        -e BITBUCKET_COMMIT="${BITBUCKET_COMMIT}" \
        -e BITBUCKET_REPO_OWNER="${BITBUCKET_REPO_OWNER}" \
        -e BITBUCKET_REPO_SLUG="$(basename $FIXTURE_DIR)"\
        -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
        --workdir=$FIXTURE_DIR \
        --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Test fails with no organization" {
    run docker run \
        -e SENTRY_AUTH_TOKEN="${SENTRY_AUTH_TOKEN}" \
        -e SENTRY_PROJECT="${PROJECT}" \
        -e BITBUCKET_COMMIT="${BITBUCKET_COMMIT}" \
        -e BITBUCKET_REPO_OWNER="${BITBUCKET_REPO_OWNER}" \
        -e BITBUCKET_REPO_SLUG="$(basename $FIXTURE_DIR)"\
        -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
        --workdir=$FIXTURE_DIR \
        --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "Test fails with no project" {
    run docker run \
        -e SENTRY_AUTH_TOKEN="${SENTRY_AUTH_TOKEN}" \
        -e SENTRY_ORG="${ORGANIZATION}" \
        -e BITBUCKET_COMMIT="${BITBUCKET_COMMIT}" \
        -e BITBUCKET_REPO_OWNER="${BITBUCKET_REPO_OWNER}" \
        -e BITBUCKET_REPO_SLUG="$(basename $FIXTURE_DIR)"\
        -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
        --workdir=$FIXTURE_DIR \
        --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}


